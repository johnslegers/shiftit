
#Shift-it

##Summary

This is just a basic implementation of a "shift-it" game I made in 2002, which nevertheless still works in modern browsers.

As the code is ancient and outdated, I'm releasing it into the public domain.


##Demo

For a live demo, go to [http://jslegers.github.com/shiftit/](http://jslegers.github.com/shiftit/).